package main

import (
	"fmt"
	"log"

	"gitlab.com/verygoodsoftwarenotvirus/judy/lib/profile"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
)

const (
	readmePathFlagName = "readme"
	defaultREADMEPath  = "./README.md"

	goModFlagName    = "mod"
	defaultGoModPath = "./go.mod"
)

var (
	ci         bool
	readmePath string
	modPath    string

	// judgeCmd represents the judge command
	judgeCmd = &cobra.Command{
		Use:   "ensure",
		Short: "Ensures there is a dependency section in the README",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			current, err := profile.ParseFromGoModFile(modPath)
			if err != nil {
				log.Fatal(err)
			}

			established, readmeErr := profile.ParseFromREADME(readmePath)
			current.AdoptExplanations(established)

			notFound := readmeErr == profile.ErrNoREADMEFound
			matching := current.DepHash() != established.DepHash()

			if !matching && ci {
				log.Fatal("README dependency table not up to date!")
			} else if readmeErr != nil && !notFound {
				log.Fatal(err)
			}

			var doTheThing bool
			if established == nil && notFound {
				doTheThing = askUserIfTheyWantToPerformAction("write a dependency section into your README")
			} else if established != nil && current.DepHash() != established.DepHash() {
				doTheThing = askUserIfTheyWantToPerformAction("overwrite the dependency section of your README")
			}

			if doTheThing {
				if err := current.WriteToREADME(readmePath); err != nil {
					log.Fatal(err)
				}
			}
		},
	}
)

func init() {
	rootCmd.AddCommand(judgeCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command and all subcommands, e.g.:
	// judgeCmd.PersistentFlags().String("foo", "", "A help for foo")

	// local flags which will only run when this command is called directly
	judgeCmd.Flags().BoolVarP(&ci, "ci", "F", false, "Call os.Exit(1) when discrepancies are found")
	judgeCmd.Flags().StringVarP(&readmePath, readmePathFlagName, "r", defaultREADMEPath, "README to parse")
	judgeCmd.Flags().StringVarP(&modPath, goModFlagName, "m", defaultGoModPath, "README to parse")
}

func askUserIfTheyWantToPerformAction(action string) bool {
	const yes = "Yes"

	prompt := promptui.Select{
		Label: fmt.Sprintf("Would you like me to %s?", action),
		Items: []string{yes, "No"},
	}
	_, result, _ := prompt.Run()

	return result == yes
}
