package main

import (
	"github.com/spf13/cobra"
	"log"
)

var (
	debug   bool
	verbose bool

	// rootCmd represents the base command when called without any subcommands
	rootCmd = &cobra.Command{
		Use:   "judy",
		Short: "determines whether or not your dependencies are justified",
		Long:  `Judy looks at your modfile and creates a markdown table in the README with space to justify each dependency.`,
		// Uncomment the following line if your bare application
		// has an action associated with it:
		//	Run: func(cmd *cobra.Command, args []string) { },
	}
)

func init() {
	rootCmd.PersistentFlags().BoolVarP(&debug, "debug", "d", false, "log select debug information")
	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "verbose output")
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
