FROM golang:latest

RUN which go

ADD scripts/copy_files.sh /copy_files.sh

ENTRYPOINT [ "/copy_files.sh" ]
