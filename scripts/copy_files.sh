#!/bin/bash

for i in modfile module semver
do
	cp -rf `which go | sed -e 's/\/bin\/go/\//'`src/cmd/go/internal/$i/ /copied
	find /copied/$i -type f -exec sed --in-place "s/cmd\/go\/internal\//gitlab\.com\/verygoodsoftwarenotvirus\/judy\/copied\//g" {} \;
	find /copied/$i -type f -exec sed --in-place "s/internal\//gitlab\.com\/verygoodsoftwarenotvirus\/judy\/copied\//g" {} \;
done

for i in lazyregexp
do
	cp -rf `which go | sed -e 's/\/bin\/go/\//'`src/internal/$i/ /copied
	find /copied/$i -type f -exec sed --in-place "s/internal\//gitlab\.com\/verygoodsoftwarenotvirus\/judy\/copied\//g" {} \;
done
