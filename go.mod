module gitlab.com/verygoodsoftwarenotvirus/judy

go 1.12

require (
	github.com/manifoldco/promptui v0.3.2
	github.com/mitchellh/hashstructure v1.0.0 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0 // indirect
	github.com/stretchr/testify v1.2.2
	gopkg.in/alecthomas/kingpin.v3-unstable v3.0.0-20180810215634-df19058c872c // indirect
)
