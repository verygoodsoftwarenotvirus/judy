GO_ROOT  := `which go | sed -e 's/\/bin\/go/\//'`
ARTIFACTS_DIR := artifacts
COVERAGE_OUT  := $(ARTIFACTS_DIR)/coverage.out
GOCOV := $(shell command -v gocov 2> /dev/null)

$(ARTIFACTS_DIR):
	mkdir -p $(ARTIFACTS_DIR)

.PHONY: prereqs
prereqs:
ifndef GOCOV
	(cd /tmp && go get github.com/axw/gocov/gocov)
endif

.PHONY: vendor-clean
vendor-clean:
	rm -rf vendor go.sum

.PHONY: vendor
vendor:
	GO111MODULE=on go mod vendor

.PHONY: revendor
revendor: vendor-clean vendor

## source code copying

ensure-copy-dir:
	mkdir -p copied

clean-copies:
	rm -rf copied

copies: ensure-copy-dir
	docker build --tag=devfilecopier --file=copies.Dockerfile .
	docker run --volume `pwd`/copied:/copied --user=`id -u` devfilecopier:latest

fresh-copies: clean-copies copies

## installation

install:
	go install gitlab.com/verygoodsoftwarenotvirus/judy/cmd/judy

## quality

test:
	for pkg in `go list ./...`; do go test -race -failfast -cover $$pkg; done

coverage: $(COVERAGE_OUT)

$(COVERAGE_OUT): prereqs $(ARTIFACTS_DIR)
	set -ex; \
	echo "mode: set" > $(COVERAGE_OUT);
	for pkg in `go list ./...`; do \
		go test -coverprofile=profile.out -v -count 5 -race -failfast $$pkg; \
		if [ $$? -ne 0 ]; then exit 1; fi; \
		cat profile.out | grep -v "mode: atomic" >> $(COVERAGE_OUT); \
	rm -f profile.out; \
	done || exit 1
	gocov convert $(COVERAGE_OUT) | gocov report
	go tool cover -html=$(COVERAGE_OUT)
