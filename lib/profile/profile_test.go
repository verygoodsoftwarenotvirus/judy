package profile

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestProfile_DepHash(T *testing.T) {
	T.Parallel()

	T.Run("happy path", func(t *testing.T) {
		p := &Profile{
			Dependencies: []Dependency{
				{
					Version: "0.0.1",
					Path:    "hey/there/hi",
				},
				{
					Version: "1.2.3",
					Path:    "oh/hello/there",
				},
			},
		}

		expected := "042690e02afcccfe88df581d112f27ada1cdcd69f812fba9e3dd89a512f001d54ad0d35f6def1c5ddcd8a51298244bcb27dad8bd57504ab2f0ee8c114c9468b0"
		actual := p.DepHash()

		assert.Equal(t, expected, actual, "expected result to equal %q, was %q", expected, actual)
	})
}
